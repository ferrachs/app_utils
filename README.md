# app-utils

This is a collection of small utilities for (mostly) building python CLIs by means of
typer (https://typer.tiangolo.com/tutorial/package/).

There might occasionally be stuff unrelated to typer, but nice to have in this pkg (e.g. `deco_init`).

## List of utilities:

### Related to typer:
* `override_default_config_with_user_config` --> Decorator to put after the `@app` typer decorator for declaring an app. Its function is to allow a hierarchy of configurations:
  * application defaults
  * user defaults
  * user runtime arguments

* `sanitize` -->  Decorator to sanitize application params in typer (mostly obsolete, now typer offers this possibility)

### Unrelated to typer:
* `rec_merge` --> Update two dicts of dicts recursively

* `deco_init` --> Decorates a class `__init__` function,to automatically handles class initialization, ie without explicit `self.some_attr = value`. Of course python Dataclasses do that as well, but Dataclasses come with more features that are sometimes undesired. Hence this tiny utility to use with basic classes.
