#!/usr/bin/env python3
# Copyright ETH Zurich
# SPDX-License-Identifier: BSD-3-Clause
# -*- coding: utf-8 -*-


'''
ETH Zurich, Sylvaine Ferrachat 2020-01

This is a collection of small utilities for (mostly) building python CLIs by means of
typer (https://typer.tiangolo.com/tutorial/package/).

There might occasionally be stuff unrelated to typer, but nice to have in this pkg (e.g. `deco_init`).
'''

#-----------------------------------------------------
#-- Initial imports
from collections.abc import Iterable, MutableMapping
from functools import wraps
from inspect import getfullargspec
import toml

import click
from click.core import ParameterSource
import typer

#-----------------------------------------------------
#-- Typer-related

def sanitize(**kwargs_san):
    """ Decorator to sanitize params (== process for specific formatting)
    Usage:
    Right above the definition of the relevant function, define the callable
    that should apply to a specific argument.
    For Example:

def str2complex_type(s: str):
    [... some code to convert a string to a `complex_type` object]
    return complex_type

@sanitize(
        v1 = str2complex_type,
        )
def main(
        v1 : str = typer.Option("some_default_string"),
        v2 : int = typer.Option(2),
        )

    Note: with recent developments of typer, this sanitizer is probably not needed anymore.
    """
    def deco(f):
        @wraps(f)
        def wrap(**kwargs):
            ctx = click.get_current_context()
            for k,sani_func in kwargs_san.items():
                try:
                    if isinstance(ctx.params[k],Iterable) and not isinstance(ctx.params[k],str):
                        ctx.params[k] = sani_func(*ctx.params[k])
                    else:
                        ctx.params[k] = sani_func(ctx.params[k])
                except KeyError:
                    typer.secho(f"{k} is not a valid param of {f.__name__}", err=True)
                    raise typer.Exit(code=1)
            co = {}
            for k,v in kwargs.items():
                if isinstance(v,click.core.Context):
                    co = {k:ctx}
                    break

            params = {**co, **ctx.params}
            res = f(**params)
            return res
        return wrap
    return deco

def override_default_config_with_user_config():
    """Decorator to put after the `@app` typer decorator for declaring an app.
    Its function is to allow a hierarchy of configurations:
       * application defaults
       * user defaults
       * user runtime arguments

    To work properly, the application must contain an argument (declared as Path)
    named 'user_cfg'.
    """
    def deco(f):
        @wraps(f)
        def wrap(**kwargs):
            ctx = click.get_current_context()
            cfg = ctx.params['cfg']
            user_cfg_file = ctx.params['user_cfg']

            #-- Update cfg with user config file
            if user_cfg_file:
                user_cfg = toml.load(user_cfg_file)
                cfg.update(**user_cfg)

            #-- Update cfg with user commandline values or vice versa
            for k,v in ctx.params.items():
                if k == 'cfg' or k == 'user_cfg':
                    #-- skip these 2 special params
                    continue
                else:
                    is_user_input = ctx._parameter_source[k] == ParameterSource.COMMANDLINE
                    if is_user_input:
                        setattr(cfg,k,v)
                    else:
                        try:
                            ctx.params[k] = getattr(cfg,k)
                        except AttributeError:
                            pass

            #-- Make sure to pass back the standard click context as is normally
            #   done when this deco is not in use
            co = {}
            for k,v in kwargs.items():
                if isinstance(v,click.core.Context):
                    co = {k:ctx}
                    break

            params = {**co, **ctx.params}
            res = f(**params)
            return res
        return wrap
    return deco

#-----------------------------------------------------
#-- Unrelated to typer

def rec_merge(d1, d2):
    '''
    Update two dicts of dicts recursively,
    if either mapping has leaves that are non-dicts,
    the second's leaf overwrites the first's.
    https://stackoverflow.com/questions/7204805/how-to-merge-dictionaries-of-dictionaries/7205107#7205107
    '''
    for k, v in d1.items():
        if k in d2:
            if all(isinstance(e, MutableMapping) for e in (v, d2[k])):
                d2[k] = rec_merge(v, d2[k])
    d3 = d1.copy()
    d3.update(d2)
    return d3

def deco_init(f):
    '''
    Automatically handles class initialization, ie without explicit 'self.some_attr = value'
    '''
    @wraps(f)
    def wrap(*args,**kwargs):
        me = args[0] # class instance
        arg_names = getfullargspec(f)[0]
        for k,v in zip(arg_names[1:],args[1:]):
            setattr(me,k,v)
        for k,v in kwargs.items():
           setattr(me,k,v) 
        return f(*args,**kwargs)
    return wrap

